private string GetUniqueId()
{
       var token = Windows.System.Profile.HardwareIdentification.GetPackageSpecificToken(null);
        IBuffer buffer = token.Id;
        using (var dataReader = DataReader.FromBuffer(buffer))
        {
                var bytes = new byte[buffer.Length];
                dataReader.ReadBytes(bytes);
                return BitConverter.ToString(bytes);
        }
}